package org.arecap.bgup.ops.console.mvp.component.verticallayout;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import org.arecap.bgup.ops.console.mvp.FlowPresenter;
import org.arecap.bgup.ops.console.mvp.FlowView;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class VerticalLayoutFlowView<P extends FlowPresenter> extends VerticalLayout implements FlowView<P> {

    @Autowired
    private P presenter;


    @Override
    public void onAttach(AttachEvent attachEvent) {
        FlowView.super.onAttach(attachEvent);
    }

    @Override
    public P getPresenter() {
        return presenter;
    }

}
