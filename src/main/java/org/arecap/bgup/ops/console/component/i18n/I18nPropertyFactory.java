package org.arecap.bgup.ops.console.component.i18n;


import org.arecap.bgup.ops.console.component.HasIdentity;

import java.io.Serializable;

public interface I18nPropertyFactory<ID extends Serializable> extends HasIdentity<ID> {

    ID getI18nId();

}
