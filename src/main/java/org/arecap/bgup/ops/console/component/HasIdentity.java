package org.arecap.bgup.ops.console.component;

import java.io.Serializable;

public interface HasIdentity<ID extends Serializable> extends Serializable {

    ID getId();

}
