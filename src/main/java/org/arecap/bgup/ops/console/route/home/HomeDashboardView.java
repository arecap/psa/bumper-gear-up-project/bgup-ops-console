package org.arecap.bgup.ops.console.route.home;

import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.arecap.bgup.ops.console.mvp.component.horizontallayout.HorizontalLayoutFlowView;

@SpringComponent
@UIScope
public class HomeDashboardView extends HorizontalLayoutFlowView<HomeDashboardPresenter> {


    @Override
    public void buildView() {
        add("Home Dashboard View");
    }
}
