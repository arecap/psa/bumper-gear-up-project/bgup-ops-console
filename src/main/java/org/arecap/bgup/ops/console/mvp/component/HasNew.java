package org.arecap.bgup.ops.console.mvp.component;

public interface HasNew<T> {

    void createItem();

}
