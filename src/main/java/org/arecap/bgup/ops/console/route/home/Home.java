package org.arecap.bgup.ops.console.route.home;

import com.vaadin.flow.router.Route;
import org.arecap.bgup.ops.console.route.template.RouteHorizontalLayout;
import org.springframework.beans.factory.annotation.Autowired;

@Route("home")
public class Home extends RouteHorizontalLayout {

    @Autowired
    private HomeDashboardView dashboardView;


    @Override
    protected void buildLayout() {
        addContentView(dashboardView);
    }
}
