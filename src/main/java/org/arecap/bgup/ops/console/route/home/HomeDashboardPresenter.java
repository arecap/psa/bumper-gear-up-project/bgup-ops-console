package org.arecap.bgup.ops.console.route.home;

import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.arecap.bgup.ops.console.mvp.DefaultFlowPresenter;

@SpringComponent
@UIScope
public class HomeDashboardPresenter extends DefaultFlowPresenter<HomeDashboardView> {
}
